import { Server } from "http"
import Application from "./Application"


process.on('unhandledRejection', up => {
    throw up
})


new Application()
    .start()
    .then((server: Server) => {
        const serverAddress = server.address();

        const bind = typeof serverAddress === "string"
            ? `pipe ${serverAddress}`
            : `port ${serverAddress.port}`;

        console.log(`Listening on ${bind}`);
    })


