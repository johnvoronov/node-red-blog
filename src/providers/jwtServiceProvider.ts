import {Services} from "kontik";
import JwtService from "../services/JwtService/JwtService";
import * as fs from 'fs';
import * as path from 'path'

const getJwtKey = (): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        fs.readFile(`${__dirname}${path.sep}..${path.sep}..${path.sep}keys/jwt.key`, (err, data) => {
            if (err) {
                return reject(err)
            }

            resolve(data)
        })
    })
}

export default async (services: Services, config: any) => {
    return new JwtService(await getJwtKey())
}
