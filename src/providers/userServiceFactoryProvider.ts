import {Services} from "kontik";
import UserServiceFactory from "../services/UserService/factories/UserServiceFactory";
import UserRepository from "../services/UserService/repositories/UserRepository";
import ExistingUserFactory from "../services/UserService/factories/ExistingUserFactory";
import UserFactory from "../services/UserService/factories/UserFactory";
import MongoRepository from "../services/MongoService/repositories/MongoRepository";

export default async (services: Services, config: any) => {
    const mongoRepository = new MongoRepository(config.db.collectionList.users, config)
    await mongoRepository.connect(`${config.db.adapter}://${config.db.host}:${config.db.port}/${config.db.name}`);

    const repository = new UserRepository(mongoRepository, config)

    const existingUserFactory = new ExistingUserFactory(
        new UserFactory()
    )

    return new UserServiceFactory(repository, repository, existingUserFactory, config)
}
