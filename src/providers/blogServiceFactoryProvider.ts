import {Services} from "kontik";
import BlogServiceFactory from "../services/BlogService/factories/BlogServiceFactory";
import BlogRepository from "../services/BlogService/repositories/BlogRepository";
import MongoRepository from "../services/MongoService/repositories/MongoRepository";


export default async(services: Services, config: any): Promise<BlogServiceFactory> => {
    const mongoRepository = new MongoRepository(config.db.collectionList.blog, config)
    await mongoRepository.connect(`${config.db.adapter}://${config.db.host}:${config.db.port}/${config.db.name}`);

    const repository = new BlogRepository(mongoRepository, config)

    return new BlogServiceFactory(repository, repository, config)
}
