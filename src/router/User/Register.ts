import { Request, Response } from "express";
import {Services} from "kontik";
import { AbstractRoute, JsonResponse, AbstractResponse } from "kudy";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import User from "../../services/UserService/models/User";
import EmailAlreadyInUseError from "../../services/UserService/errors/EmailAlreadyInUseError";
import UserRegistrationError from "../../services/UserService/errors/UserRegistrationError";

class Register extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v0/registration";
    }

    get bodySchema() {
        return {
            type: 'object',
            properties: {
                email: {
                    type: 'string'
                },
                password: {
                    type: 'string'
                },
                name: {
                    type: 'string'
                }
            },
            required: ['email', 'password', 'name']
        }
    }

    async handler(req: Request, res: Response): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const { email, name, password } = req.body;

        const user = new User(email, name, new Date(), new Date())

        try {
            await userService.register(user, password)
        } catch (e) {
            if (e instanceof EmailAlreadyInUseError) {
                return new JsonResponse({
                    error: 'email_already_in_use',
                    error_description: 'Email already in use.'
                }, 401);
            }

            if (e instanceof UserRegistrationError) {
                return new JsonResponse({
                    error: 'user_registration',
                    error_description: 'Unknown user registration.'
                }, 400);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            message: 'New user is successfully registered'
        }, 201);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new Register(routeConstructParams)
}

