import { Request, Response } from "express";
import {Services} from "kontik";
import { AbstractRoute, JsonResponse, AbstractResponse } from "kudy";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import BlogServiceFactory from "../../services/BlogService/factories/BlogServiceFactory";
import JwtService from "../../services/JwtService/JwtService";
import BlogCreationError from "../../services/BlogService/errors/BlogCreationError";
import BlogUserNotFoundError from "../../services/BlogService/errors/BlogUserNotFoundError";


export default class Create extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method() {
        return 'POST';
    }

    get path() {
        return '/v0/blog/Create';
    }

    get bodySchema() {
        return {
            type: 'object',
            properties: {
                content: {
                    type: 'string'
                }
            },
            required: ['content']
        }
    }

    async handler(req: Request, res: Response) {
        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')
        let token = req.get('authorization')

        if (!token) {
            return new JsonResponse({
                error: 'access_denied',
                error_description: 'Face match access denied.'
            }, 403);
        }

        const isToken = token.search('Bearer')
        if (isToken === -1) {
            return new JsonResponse({
                error: 'bearer_token',
                error_description: 'Invalid bearer token error.'
            }, 400);
        }

        token = token.replace('Bearer ', '')

        const { userId } = await jwtService.getJwtData(token)

        const { content } = req.body

        const blogServiceFactory =
            await this.services.getService<BlogServiceFactory>('blogServiceFactoryProvider')
        const blogService = await blogServiceFactory.create()

        try {
            const blog = await blogService.createBlog(userId, content)

            if (!blog) {
                throw new BlogCreationError()
            }

            const blogList = await blogService.getBlogListByUserId(userId)

            if (!blogList || blogList.length === 0) {
                throw new BlogUserNotFoundError()
            }

            return await new JsonResponse({
                response: blogList
            }, 200);
        } catch (e) {
            if (e instanceof BlogCreationError) {
                return new JsonResponse({
                    error: 'blog_creation',
                    error_description: 'Blog creation was failed.'
                }, 400);
            }

            if (e instanceof BlogUserNotFoundError) {
                return new JsonResponse({
                    error: 'blog_not_found',
                    error_description: 'Blog not found.'
                }, 400);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }
    }
}


