import { Request, Response } from "express";
import {Services} from "kontik";
import { AbstractRoute, JsonResponse, AbstractResponse } from "kudy";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import BlogServiceFactory from "../../services/BlogService/factories/BlogServiceFactory";
import JwtService from "../../services/JwtService/JwtService";
import BlogCreationError from "../../services/BlogService/errors/BlogCreationError";
import BlogUserNotFoundError from "../../services/BlogService/errors/BlogUserNotFoundError";
import BlogNotFoundError from "../../services/BlogService/errors/BlogNotFoundError";


export default class GetList extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method() {
        return 'GET';
    }

    get path() {
        return '/v0/blog/GetList';
    }

    async handler(req: Request, res: Response) {
        const blogServiceFactory =
            await this.services.getService<BlogServiceFactory>('blogServiceFactoryProvider')
        const blogService = await blogServiceFactory.create()

        try {
            const blogList = await blogService.getBlogList()

            if (!blogList || blogList.length === 0) {
                throw new BlogNotFoundError()
            }

            return await new JsonResponse({
                response: blogList
            }, 200);
        } catch (e) {
            if (e instanceof BlogNotFoundError) {
                return new JsonResponse({
                    error: 'blog_not_found',
                    error_description: 'Blog not found.'
                }, 400);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }
    }
}


