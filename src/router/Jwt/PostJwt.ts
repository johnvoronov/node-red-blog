import { Request, Response } from "express";
import {Services} from "kontik";
import { AbstractRoute, JsonResponse, AbstractResponse } from "kudy";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import ExistingUser from "../../services/UserService/models/ExistingUser";
import InvalidUserCredentialsError from "../../services/UserService/errors/InvalidUserCredentialsError";
import JwtService from "../../services/JwtService/JwtService";

class PostJwt extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v0/jwt";
    }

    get bodySchema() {
        return {
            type: 'object',
            properties: {
                email: {
                    type: 'string'
                },
                password: {
                    type: 'string'
                },
            },
            required: ['email', 'password']
        }
    }

    async handler(req: Request, res: Response): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const { email, password } = req.body;

        let user: ExistingUser

        try {
            user = await userService.getUserByCredentials(email, password)
        } catch (e) {
            if (e instanceof InvalidUserCredentialsError) {
                return new JsonResponse({
                    error: 'invalid_credentials',
                    error_description: 'Provided credentials are invalid.'
                }, 401);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')

        const payload = {
            userId: user.id
        }

        const jwt = await jwtService.sign(payload)

        return new JsonResponse({
            jwt
        }, 200);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new PostJwt(routeConstructParams)
}

