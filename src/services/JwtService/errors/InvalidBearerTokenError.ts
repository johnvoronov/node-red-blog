import JwtServiceError from "./JwtServiceError";

export default class InvalidBearerTokenError extends JwtServiceError {
    constructor() {
        super()

        Object.setPrototypeOf(this, InvalidBearerTokenError.prototype)
    }
}
