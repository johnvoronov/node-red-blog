export default class JwtServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, JwtServiceError.prototype)
    }
}
