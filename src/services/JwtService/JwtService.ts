import * as jwt from 'jsonwebtoken'


interface JwtPayload {
    userId: string
}


interface JwtData {
    userId: string
}


export default class JwtService {
    constructor(
        protected readonly privateKey: Buffer
    ) {
    }

    public async sign(payload: {[p: string]: any}): Promise<string> {
        return jwt.sign(payload, this.privateKey)
    }

    public async getJwtData(jwtToken: string): Promise<JwtData> {
        return this.decodeJwt(jwtToken)
    }

    protected async decodeJwt(jwtToken: string): Promise<JwtPayload> {
        return jwt.verify(jwtToken, await this.privateKey) as JwtPayload
    }

}
