import * as uuid from "uuid/v4"
import User from "./models/User";
import UserRepositoryReader from "./interfaces/UserRepositoryReader";
import UserRepositoryWriter from "./interfaces/UserRepositoryWriter";
import ExistingUserFactory from "./factories/ExistingUserFactory";
import ExistingUser from "./models/ExistingUser";
import UserData from "./interfaces/UserData";


export default class UserService {
    constructor(
        protected readonly userRepositoryReader: UserRepositoryReader,
        protected readonly userRepositoryWriter: UserRepositoryWriter,
        protected readonly existingUserFactory: ExistingUserFactory,
        protected readonly config: any
    ) {

    }

    public async register(user: User, password: string): Promise<void> {
        return this.registerUser(user, password)
    }

    public async getUserByCredentials(email: string, password: string): Promise<ExistingUser> {
        const existingUserData = await this.userRepositoryReader.getUserByCredentials(email, password)

        return this.existingUserFactory.create(existingUserData)
    }

    public async getUserById(userId: string): Promise<ExistingUser> {
        const existingUserData = await this.userRepositoryReader.getUserById(userId)

        return this.existingUserFactory.create(existingUserData)
    }

    protected async registerUser(user: User, password: string): Promise<void> {
        const data: UserData = {
            id: uuid(),
            ...user,
            password,
        }

        await this.userRepositoryWriter.saveUser(data)
    }
}
