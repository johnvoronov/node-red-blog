export default class ExistingUser {
    constructor(
        public readonly id: string,
        public readonly email: string,
        public readonly name: string,
        public readonly createdAt: Date,
        public readonly updatedAt: Date,
    ) {}
}
