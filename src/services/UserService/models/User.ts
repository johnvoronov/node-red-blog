export default class User {
    constructor(
        public readonly email: string,
        public readonly name: string,
        public readonly createdAt: Date,
        public readonly updatedAt: Date,
    ) { }
}
