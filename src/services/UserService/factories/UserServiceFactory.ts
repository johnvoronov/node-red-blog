import UserService from "../UserService";
import UserRepositoryReader from "../interfaces/UserRepositoryReader";
import UserRepositoryWriter from "../interfaces/UserRepositoryWriter";
import ExistingUserFactory from "./ExistingUserFactory";

export default class UserServiceFactory {
    constructor(
        protected readonly userRepositoryReader: UserRepositoryReader,
        protected readonly userRepositoryWriter: UserRepositoryWriter,
        protected readonly existingUserFactory: ExistingUserFactory,
        protected readonly config: any
    ) {

    }

    async create(): Promise<UserService> {
        return new UserService(this.userRepositoryReader, this.userRepositoryWriter, this.existingUserFactory, this.config)
    }
}
