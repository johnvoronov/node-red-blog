import {expect} from 'chai'
import ExistingUserData from "../interfaces/ExistingUserData";
import ExistingUserFactory from "./ExistingUserFactory";
import * as sinon from 'sinon'
import User from "../models/User";

describe('ExistingUserFactory', function () {
    it('should create ExistingUser model from data', async function () {
        const FAKE_USER_DATA: ExistingUserData = {
            id: Math.random().toString(36).substring(7),
            email: 'john@doe.com',
            name: 'John',
            createdAt: new Date(),
            updatedAt: new Date()
        }

        const FAKE_USER_FACTORY: any = {
            create: sinon.spy(() => {
                return new User(
                    'john@doe.com',
                    'John',
                    new Date(),
                    new Date()
                )
            })
        }

        const factory = new ExistingUserFactory(FAKE_USER_FACTORY)

        const existingUser = await factory.create(FAKE_USER_DATA)

        expect(existingUser.id).to.be.eql(FAKE_USER_DATA.id)
    })
})
