import ExistingUserData from "../interfaces/ExistingUserData";
import UserFactory from "./UserFactory";
import {expect} from 'chai'
import User from "../models/User";
import MissingUserDataError from "../errors/MissingUserDataError";


const FAKE_USER_DATA: ExistingUserData = {
    id: Math.random().toString(36).substring(7),
    email: 'john@doe.com',
    name: 'John',
    createdAt: new Date(),
    updatedAt: new Date(),
}

describe('UserFactory', function () {
    it('should create user', async function () {
        const factory = new UserFactory()

        const user = await factory.create(FAKE_USER_DATA)

        expect(user).to.be.instanceof(User)
    })

    it('should throw missing data error for user', async function () {
        const factory = new UserFactory()

        const FAKE_MISSING_DATA = {
            ...FAKE_USER_DATA
        }

        delete FAKE_MISSING_DATA.name

        try {
            const user = await factory.create(FAKE_MISSING_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(MissingUserDataError)
            return
        }

        throw new Error('should never happened')
    })
})
