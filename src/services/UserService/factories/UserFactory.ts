import ExistingUserData from "../interfaces/ExistingUserData";
import User from "../models/User";
import MissingUserDataError from "../errors/MissingUserDataError";

export default class UserFactory {
    public async create(userData: ExistingUserData): Promise<User> {
        return this.createUser(userData)
    }

    protected async createUser(userData: ExistingUserData): Promise<User> {
        const { email, name, createdAt, updatedAt } = userData

        if (!email || !name || !createdAt || !updatedAt) {
            throw new MissingUserDataError()
        }

        return new User(
            email,
            name,
            createdAt,
            updatedAt
        )
    }
}
