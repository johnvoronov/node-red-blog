import {expect} from 'chai'
import UserServiceFactory from "./UserServiceFactory";
import UserService from "../UserService";

describe('UserServiceFactory', function () {
    it('should create UserService', async function () {
        const FAKE_REPOSITORY: any = {}

        const factory = new UserServiceFactory(FAKE_REPOSITORY, FAKE_REPOSITORY, {} as any, {} as any)

        const userService = await factory.create()

        expect(userService).to.be.instanceof(UserService)
    })
})
