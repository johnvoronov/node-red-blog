import { expect } from 'chai'
import * as sinon from 'sinon'
import UserService from "./UserService";
import UserData from "./interfaces/UserData";
import User from "./models/User";
import ExistingUser from "./models/ExistingUser";

describe('UserService', function () {
    it ('should save correct user to repository', async function () {
        const FAKE_USER_REPOSITORY_WRITER: any = {
            saveUser: sinon.spy(async (userData: UserData) => {})
        }

        const userService = new UserService({} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_USER = new User(
            'john@doe.com',
            'John',
            new Date(),
            new Date(),
        )

        await userService.register(FAKE_USER, 'verySecretMuchWow')

        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.calledOnce).to.be.eql(true)
    })

    it('should return user by credentials', async function () {
        const FAKE_USER = new ExistingUser(
            Math.random().toString(36).substring(7),
            'john@doe.com',
            'John',
            new Date(),
            new Date(),
        )

        const FAKE_EXISTING_USER_FACTORY: any = {
            create: sinon.spy(async () => FAKE_USER)
        }

        const FAKE_USER_REPOSITORY_READER: any = {
            // getUserByCredentials(email: string, password: string): Promise<ExistingUserData>
            getUserByCredentials: sinon.spy(async (email: string, password: string) => {})
        }

        const service = new UserService(FAKE_USER_REPOSITORY_READER, {} as any, FAKE_EXISTING_USER_FACTORY, {} as any)

        const existingUser = await service.getUserByCredentials('john@doe.com', 'superSecret')

        expect(existingUser).to.be.instanceof(ExistingUser)
        expect(existingUser).to.be.equal(FAKE_USER)
    })

    it('should return user by id', async function () {
        const FAKE_USER = new ExistingUser(
            Math.random().toString(36).substring(7),
            'john@doe.com',
            'John',
            new Date(),
            new Date(),
        )

        const FAKE_EXISTING_USER_FACTORY: any = {
            create: sinon.spy(async () => FAKE_USER)
        }

        const FAKE_USER_REPOSITORY_READER: any = {
            getUserById: sinon.spy(async (userId: string) => {})
        }

        const service = new UserService(FAKE_USER_REPOSITORY_READER, {} as any, FAKE_EXISTING_USER_FACTORY, {})

        const existingUser = await service.getUserById(FAKE_USER.id)

        expect(existingUser).to.be.instanceof(ExistingUser)
        expect(existingUser).to.be.equal(FAKE_USER)
    })
})
