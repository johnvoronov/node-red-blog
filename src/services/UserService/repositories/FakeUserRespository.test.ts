import { expect } from 'chai'
import NewUserData from "../interfaces/NewUserData";
import FakeUserRepository from "./FakeUserRepository";
import UserIdNotFoundError from "../errors/UserIdNotFoundError";
import InvalidUserCredentialsError from "../errors/InvalidUserCredentialsError";
import UserEmailNotFoundError from "../errors/UserEmailNotFoundError";
import EmailAlreadyInUseError from "../errors/EmailAlreadyInUseError";

const FAKE_USER_DATA: NewUserData = {
    email: 'john@doe.com',
    name: 'John',
    password: Buffer.from(new Date().toISOString(), 'utf8').toString('hex'),
    createdAt: new Date(),
    updatedAt: new Date()
}

const userRepository = new FakeUserRepository()

describe('FakeUserRepository', function () {
    let userId: string

    it ('should save new user and return correct data', async function () {
        const existingUser = await userRepository.saveUser(FAKE_USER_DATA)

        expect(existingUser.email).to.be.eql(FAKE_USER_DATA.email)
        expect(existingUser.id).to.exist

        userId = existingUser.id
    })

    it('should return correct user data by id', async function () {
        const existingUser = await userRepository.getUserById(userId)


        expect(existingUser.email).to.be.eql(FAKE_USER_DATA.email)
    })

    it('should throw error when user not found', async function () {
        try {
            const existingUser = await userRepository.getUserById('someRandomIdHopeItIsNotSet')
        } catch (e) {
            expect(e).to.be.instanceof(UserIdNotFoundError)
            return
        }

        throw new Error('should never happened')
    })

    it('should return existing user data by correct credentials', async function () {
        const existingUser = await userRepository.getUserByCredentials(FAKE_USER_DATA.email, FAKE_USER_DATA.password)

        expect(existingUser.email).to.be.eql(FAKE_USER_DATA.email)
    })

    it('should throw error with wrong credentials', async function () {
        try {
            const existingUser = await userRepository.getUserByCredentials(FAKE_USER_DATA.email, 'wrongPassword')
        } catch (e) {
            expect(e).to.be.instanceof(InvalidUserCredentialsError)
            return
        }

        throw new Error('should never happened')
    })

    it('should throw error when try save user with existing email', async function () {
        try {
            const existingUser = await userRepository.saveUser(FAKE_USER_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(EmailAlreadyInUseError)
            return
        }

        throw new Error('should never happened')
    })

    it('should throw error with wrong email', async function () {
        try {
            const existingUser = await userRepository.getUserByCredentials('fake@email.com', 'wrongPassword')
        } catch (e) {
            expect(e).to.be.instanceof(UserEmailNotFoundError)
            return
        }

        throw new Error('should never happened')
    })

    it('should update user and return new data', async function () {
        const NEW_EMAIL = 'new@email.com'

        const newData = {
            ...FAKE_USER_DATA,
            email: NEW_EMAIL
        }

        const existingUser = await userRepository.updateUser(userId, newData)

        expect(existingUser.id).to.be.eql(userId)
        expect(existingUser.email).to.be.eql(NEW_EMAIL)
    })

    it('should return that id was not found with unknown userId', async function () {
        try {
            const existingUser = await userRepository.updateUser('some-fake-id', FAKE_USER_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(UserIdNotFoundError)
            return
        }

        throw new Error('should never happened')
    })
})
