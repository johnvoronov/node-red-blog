import * as crypto from "crypto";
import UserRepositoryWriter from "../interfaces/UserRepositoryWriter";
import UserRepositoryReader from "../interfaces/UserRepositoryReader";
import ExistingUserData from "../interfaces/ExistingUserData";
import UserData from "../interfaces/UserData";
import UserIdNotFoundError from "../errors/UserIdNotFoundError";
import EmailAlreadyInUseError from "../errors/EmailAlreadyInUseError";
import UserRegistrationError from "../errors/UserRegistrationError";
import InvalidUserCredentialsError from "../errors/InvalidUserCredentialsError";
import MongoRepository from "../../MongoService/repositories/MongoRepository";

interface PasswordData {
    readonly hash: string,
    readonly salt: string
}

type StoredUser = ExistingUserData & { readonly password: PasswordData }

export default class UserRepository implements UserRepositoryReader, UserRepositoryWriter {
    protected readonly mongoRepository: MongoRepository
    protected readonly salt = "d80f6183-eb29-497a-b51e-4d14a009021d";

    constructor(mongoRepository: MongoRepository, config: any) {
        this.mongoRepository = mongoRepository
    }

    public async saveUser(userData: any): Promise<ExistingUserData> {
        let user = await this.mongoRepository.getDataByParams({ email: userData.email })

        if (user) {
            throw new EmailAlreadyInUseError(userData.email)
        }

        // Process password to hash
        userData.password = this.passwordToHash(userData.password)

        user = await this.mongoRepository.saveData(userData)

        if (!user) {
            throw new UserRegistrationError(userData)
        }

        return user
    }

    public async updateUser(id: string, userData: UserData): Promise<ExistingUserData> {
        return this.mongoRepository.updateData(id, userData)
    }

    public async getUserById(id: string): Promise<ExistingUserData> {
        const user = await this.mongoRepository.getDataById(id);

        if (!user) {
            throw new UserIdNotFoundError(id)
        }

        return user
    }

    async getUserByCredentials(email: string, password: string): Promise<ExistingUserData> {
        const hash = this.passwordToHash(password);

        const user = await this.mongoRepository.getDataByParams({ email, password: hash })

        if (this.passwordToHash(password) !== user.password) {
            throw new InvalidUserCredentialsError()
        }

        return user
    }

    async getUserByEmail(email: string): Promise<ExistingUserData> {
        return this.mongoRepository.getDataByParams({ email })
    }

    protected passwordToHash(password: string): string {
        const hash = crypto.createHmac('sha512', this.salt);
        hash.update(password);

        return hash.digest('hex');
    }
}
