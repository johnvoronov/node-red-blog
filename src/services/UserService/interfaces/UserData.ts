export default interface UserData {
    readonly id: string
    readonly email: string
    readonly password: string
    readonly name: string
    readonly createdAt: Date
    readonly updatedAt: Date
}
