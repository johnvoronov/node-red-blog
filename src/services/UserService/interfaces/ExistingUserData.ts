import UserData from "./UserData";

export default interface ExistingUserData extends UserData {
    id: string
}
