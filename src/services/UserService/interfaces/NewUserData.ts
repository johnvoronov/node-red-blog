import UserData from "./UserData";

export default interface NewUserData extends UserData {
    password: string
}