import UserRegistrationError from "./UserRegistrationError";

export default class UnknownUserTypeError extends UserRegistrationError {
    constructor(type: any) {
        super(`Unknown user type: ${type.constructor.name}`)

        Object.setPrototypeOf(this, UnknownUserTypeError.prototype)
    }
}