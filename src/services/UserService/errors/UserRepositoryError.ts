import UserServiceError from "./UserServiceError";

export default class UserRepositoryError extends UserServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, UserRepositoryError.prototype)
    }
}