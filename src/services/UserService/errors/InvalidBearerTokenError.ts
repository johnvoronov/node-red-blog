import UserNotFoundError from "./UserNotFoundError";

export default class InvalidUserCredentialsError extends UserNotFoundError {
    constructor() {
        super()

        Object.setPrototypeOf(this, InvalidUserCredentialsError.prototype)
    }
}