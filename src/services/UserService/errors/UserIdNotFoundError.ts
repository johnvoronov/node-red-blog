import UserNotFoundError from "./UserNotFoundError";

export default class UserIdNotFoundError extends UserNotFoundError {
    constructor(id: string) {
        super(`User with id "${id}" was not found`)

        Object.setPrototypeOf(this, UserIdNotFoundError.prototype)
    }
}
