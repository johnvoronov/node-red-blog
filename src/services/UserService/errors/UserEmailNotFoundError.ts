import UserNotFoundError from "./UserNotFoundError";

export default class UserEmailNotFoundError extends UserNotFoundError {
    constructor(email: string) {
        super(`User with email "${email}" was not found`)

        Object.setPrototypeOf(this, UserEmailNotFoundError.prototype)
    }
}
