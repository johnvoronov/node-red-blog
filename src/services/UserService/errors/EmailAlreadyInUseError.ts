import UserRepositoryError from "./UserRepositoryError";

export default class EmailAlreadyInUseError extends UserRepositoryError {
    constructor(email: string) {
        super(`Email ${email} is already in use`)

        Object.setPrototypeOf(this, EmailAlreadyInUseError.prototype)
    }
}