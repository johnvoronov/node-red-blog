export default class UserServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, UserServiceError.prototype)
    }
}