import UserServiceError from "./UserServiceError";

export default class UserNotFoundError extends UserServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, UserNotFoundError.prototype)
    }
}