import BlogData from "./BlogData";
import ExistingBlogData from "./ExistingBlogData";


export default interface BlogRepositoryWriter {
    /**
     *
     * @param blogData
     * @throws BlogCreationError
     * @throws BlogRepositoryError
     */
    saveBlog(blogData: BlogData): Promise<ExistingBlogData>
}
