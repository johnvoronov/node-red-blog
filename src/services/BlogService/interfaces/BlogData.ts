export default interface BlogData {
     readonly id: string,
     readonly userId: string,
     readonly content: string,
     readonly createdAt: Date,
     readonly updatedAt: Date
}
