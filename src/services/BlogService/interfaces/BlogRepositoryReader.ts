import ExistingBlogData from "./ExistingBlogData";


export default interface BlogRepositoryReader {
    /**
     *
     * @param id
     * @throws BlogIdNotFoundError
     * @throws BlogRepositoryError
     */
    getBlogById(id: string): Promise<ExistingBlogData>

    /**
     *
     * @param userId
     * @throws BlogUserNotFoundError
     * @throws BlogRepositoryError
     */
    getBlogListByUserId(userId: string): Promise<ExistingBlogData[]>

    /**
     *
     * @throws BlogNotFoundError
     * @throws BlogRepositoryError
     */
    getBlogList(): Promise<ExistingBlogData[]>
}
