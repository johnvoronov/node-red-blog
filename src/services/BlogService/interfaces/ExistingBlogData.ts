import BlogData from "./BlogData";

export default interface ExistingBlogData extends BlogData {
    readonly id: string,
    readonly userId: string,
    readonly content: string,
    readonly createdAt: Date,
    readonly updatedAt: Date
}
