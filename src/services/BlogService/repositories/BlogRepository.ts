import BlogData from "../interfaces/BlogData";
import ExistingBlogData from "../interfaces/ExistingBlogData";
import BlogRepositoryReader from "../interfaces/BlogRepositoryReader";
import BlogRepositoryWriter from "../interfaces/BlogRepositoryWriter";
import MongoRepository from "../../MongoService/repositories/MongoRepository";
import BlogIdNotFoundError from "../errors/BlogIdNotFoundError";
import BlogUserNotFoundError from "../errors/BlogUserNotFoundError";
import BlogNotFoundError from "../errors/BlogNotFoundError";
import BlogCreationError from "../errors/BlogCreationError";


export default class BlogRepository implements BlogRepositoryReader, BlogRepositoryWriter {
    protected readonly mongoRepository: MongoRepository

    constructor(mongoRepository: MongoRepository, config: any) {
        this.mongoRepository = mongoRepository
    }

    public async saveBlog(blogData: BlogData): Promise<ExistingBlogData> {
        const response = await this.mongoRepository.saveData(blogData)

        if (!response) {
            throw new BlogCreationError(blogData)
        }

        return response
    }

    public async getBlogById(id: string): Promise<ExistingBlogData> {
        const response = await this.mongoRepository.getDataById(id);

        if (!response) {
            throw new BlogIdNotFoundError(id)
        }

        return response
    }

    public async getBlogListByUserId(userId: string): Promise<ExistingBlogData[]> {
        const response = await this.mongoRepository.getDataListByParams({ userId });

        if (!response || response.length === 0) {
            throw new BlogUserNotFoundError(userId)
        }

        return response
    }

    public async getBlogList(): Promise<ExistingBlogData[]> {
        const response = await this.mongoRepository.getDataListByParams({});

        if (!response || response.length === 0) {
            throw new BlogNotFoundError()
        }

        return response
    }
}
