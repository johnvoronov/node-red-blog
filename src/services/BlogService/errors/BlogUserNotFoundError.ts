import BlogServiceError from "./BlogServiceError";

export default class BlogUserNotFoundError extends BlogServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogUserNotFoundError.prototype)
    }
}
