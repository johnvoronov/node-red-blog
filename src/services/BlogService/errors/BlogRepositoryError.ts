import BlogServiceError from "./BlogServiceError";

export default class BlogRepositoryError extends BlogServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogRepositoryError.prototype)
    }
}
