import BlogServiceError from "./BlogServiceError";

export default class BlogCreationError extends BlogServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogCreationError.prototype)
    }
}
