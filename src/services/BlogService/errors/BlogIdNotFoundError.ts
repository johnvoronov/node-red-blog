import BlogServiceError from "./BlogServiceError";

export default class BlogIdNotFoundError extends BlogServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogIdNotFoundError.prototype)
    }
}
