import BlogServiceError from "./BlogServiceError";

export default class BlogNotFoundError extends BlogServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogNotFoundError.prototype)
    }
}
