export default class BlogServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, BlogServiceError.prototype)
    }
}
