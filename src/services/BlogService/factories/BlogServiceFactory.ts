import BlogService from "../BlogService";
import BlogRepositoryReader from "../interfaces/BlogRepositoryReader";
import BlogRepositoryWriter from "../interfaces/BlogRepositoryWriter";


export default class BlogServiceFactory {
    constructor(
        protected readonly blogRepositoryReader: BlogRepositoryReader,
        protected readonly blogRepositoryWriter: BlogRepositoryWriter,
        protected readonly config: any
    ) { }

    async create(): Promise<BlogService> {
        return new BlogService(this.blogRepositoryReader, this.blogRepositoryWriter, this.config)
    }
}
