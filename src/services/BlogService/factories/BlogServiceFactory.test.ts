import { expect } from 'chai'
import BlogServiceFactory from "./BlogServiceFactory";
import BlogService from "../BlogService";

describe('BlogServiceFactory', function () {
    it('should create BlogService', async function () {
        const FAKE_REPOSITORY: any = {}

        const factory = new BlogServiceFactory(FAKE_REPOSITORY, FAKE_REPOSITORY, {} as any)

        const verificationService = await factory.create()

        expect(verificationService).to.be.instanceof(BlogService)
    })
})
