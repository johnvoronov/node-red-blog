export default class Blog {
    constructor(
        public readonly id: string,
        public readonly userId: string,
        public readonly content: string,
        public readonly createdAt: Date,
        public readonly updatedAt: Date,
    ) { }
}
