import * as uuid from "uuid/v4"
import ExistingBlog from "./models/ExistingBlog";
import BlogRepositoryReader from "./interfaces/BlogRepositoryReader";
import BlogRepositoryWriter from "./interfaces/BlogRepositoryWriter";
import BlogData from "./interfaces/BlogData";


export default class BlogService {
    constructor(
        protected readonly blogRepositoryReader: BlogRepositoryReader,
        protected readonly blogRepositoryWriter: BlogRepositoryWriter,
        protected readonly config: any
    ) { }

    public async createBlog(userId: string, content: string): Promise<ExistingBlog> {
        const data: BlogData = {
            id: uuid(),
            userId,
            content,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        await this.blogRepositoryWriter.saveBlog(data)

        return data
    }

    public async getBlogById(id: string): Promise<ExistingBlog> {
        return this.blogRepositoryReader.getBlogById(id)
    }

    public async getBlogListByUserId(userId: string): Promise<ExistingBlog[]> {
        return this.blogRepositoryReader.getBlogListByUserId(userId)
    }

    public async getBlogList(): Promise<ExistingBlog[]> {
        return this.blogRepositoryReader.getBlogList()
    }
}


