import * as mongo from 'mongodb';
import MongoRepositoryReader from "../interfaces/MongoRepositoryReader";
import MongoRepositoryWriter from "../interfaces/MongoRepositoryWriter";
import MongoServiceError from "../errors/MongoServiceError";
import MongoInvalidConnectionError from "../errors/MongoInvalidConnectionError";


export default class MongoRepository implements MongoRepositoryReader, MongoRepositoryWriter {
    private client: mongo.MongoClient;
    private collection: any
    private readonly config: any

    constructor(collection: string, config: any) {
        this.config = config
        this.collection = collection
    }

    // Rewrite to singleton
    public async connect(url: string): Promise<void> {
        try {
            this.client = await mongo.MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            console.log(`Connected to Mongo with name ${this.config.db.name}`);
        } catch (err) {
            if (err.message === 'Invalid connection string') {
                throw new MongoInvalidConnectionError()
            }

            throw new MongoServiceError()
        }
    }

    public disconnect(): void {
        this.client.close();
    }

    public async saveData(data: any): Promise<any> {
        return this.getCollection().insertOne(data);
    }

    public async updateData(id: string, data: any): Promise<any> {
        return this.getCollection().updateOne({ id }, { $set: data });
    }

    public async getDataById(id: string): Promise<any> {
        return this.getCollection().findOne({ id });
    }

    public async getDataByParams(data: object): Promise<any> {
        return this.getCollection().findOne(data);
    }

    public async getDataListByParams(data: object): Promise<any> {
        return this.getCollection().find(data).sort( { createdAt: -1 } ).toArray();
    }

    private getCollection(): any {
        return this.client.db(this.config.db.name).collection(this.collection)
    }
}
