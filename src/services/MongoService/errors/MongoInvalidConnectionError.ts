import MongoServiceError from "./MongoServiceError";

export default class MongoInvalidConnectionError extends MongoServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MongoInvalidConnectionError.prototype)
    }
}
