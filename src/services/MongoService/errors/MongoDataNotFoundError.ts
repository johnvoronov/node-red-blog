import MongoServiceError from "./MongoServiceError";

export default class MongoDataNotFoundError extends MongoServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MongoServiceError.prototype)
    }
}
