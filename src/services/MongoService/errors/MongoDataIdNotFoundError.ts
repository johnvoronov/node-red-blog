import MongoDataNotFoundError from "./MongoDataNotFoundError";

export default class MongoDataIdNotFoundError extends MongoDataNotFoundError {
    constructor(id: string) {
        super(`Data with id "${id}" was not found`)

        Object.setPrototypeOf(this, MongoDataIdNotFoundError.prototype)
    }
}
