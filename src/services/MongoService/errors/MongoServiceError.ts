export default class MongoServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MongoServiceError.prototype)
    }
}
