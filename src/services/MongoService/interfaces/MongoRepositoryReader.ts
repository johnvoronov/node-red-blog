export default interface MongoRepositoryReader {
    /**
     *
     * @param id
     * @throws MongoDataIdNotFoundError
     * @throws MongoDataRepositoryError
     */
    getDataById(id: string): Promise<any>

    /**
     *
     * @param data
     */
    getDataByParams(data: object): Promise<any>

    /**
     *
     * @param data
     */
    getDataListByParams(data: object): Promise<any>
}
