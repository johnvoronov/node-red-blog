export default interface MongoRepositoryWriter {
    /**
     *
     * @param data
     * @throws MongoDataRepositoryError
     */
    saveData(data: any): Promise<any>

    /**
     *
     * @param id
     * @param data
     * @throws MongoDataRepositoryError
     */
    updateData(id: string, data: any): Promise<any>
}
