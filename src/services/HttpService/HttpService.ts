import * as express from "express"
import * as bodyparser from 'body-parser'
import Loader from "kudy"
import RouteConstructParams from "./models/RouteConstructParams";
import {Server} from "http";

export default class HttpService {
    public async start(routeConstructParams: RouteConstructParams, port?: number): Promise<Server> {
        const app = express()
        app.use(bodyparser())

        const routesLoader = new Loader<RouteConstructParams>(routeConstructParams)
        routesLoader.appendRoutesFromDir(app, `${__dirname}/../../router`)

        return new Promise((resolve, reject) => {
            const server = app.listen(port, () => {
                resolve(server)
            })
        })
    }
}
