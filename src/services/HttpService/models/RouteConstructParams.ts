import {Services} from "kontik";

export default class RouteConstructParams {
    constructor(
        public readonly services: Services,
        public readonly config: any
    ) {}
}